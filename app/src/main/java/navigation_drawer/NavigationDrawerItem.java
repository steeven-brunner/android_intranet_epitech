package navigation_drawer;


public class NavigationDrawerItem {
    private int mSectionIcon;
    private String mSectionString;

    public NavigationDrawerItem(int iconId, String str) {
        mSectionIcon = iconId;
        mSectionString = str;
    }

    public int getSectionIcon() {
        return mSectionIcon;
    }

    public String getSectionString() {
        return mSectionString;
    }
}
