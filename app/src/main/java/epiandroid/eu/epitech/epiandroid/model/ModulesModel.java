package epiandroid.eu.epitech.epiandroid.model;

import com.google.gson.annotations.SerializedName;

public class ModulesModel {

    @SerializedName("modules")
    private ModuleItem[] modules;

    public void setModuleItem(ModuleItem[] modules) {
        this.modules = modules;
    }

    public ModuleItem[] getModuleItem() {
        return this.modules;
    }
}
