package epiandroid.eu.epitech.epiandroid.epitech_service;

public class EpitechServiceException extends Exception {
    public EpitechServiceException(String message) {
        super(message);
    }
}
