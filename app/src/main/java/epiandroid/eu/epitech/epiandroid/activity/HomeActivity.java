package epiandroid.eu.epitech.epiandroid.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import epiandroid.eu.epitech.epiandroid.CircleTransform;
import epiandroid.eu.epitech.epiandroid.R;
import epiandroid.eu.epitech.epiandroid.adapter.EpiAndroidNavigationAdapter;
import epiandroid.eu.epitech.epiandroid.epitech_service.EpitechService;
import epiandroid.eu.epitech.epiandroid.epitech_service.GsonResponseHandler;
import epiandroid.eu.epitech.epiandroid.fragment.DashboardFragment;
import epiandroid.eu.epitech.epiandroid.fragment.MarksFragment;
import epiandroid.eu.epitech.epiandroid.fragment.ModulesFragment;
import epiandroid.eu.epitech.epiandroid.fragment.PlanningFragment;
import epiandroid.eu.epitech.epiandroid.fragment.SettingsFragment;
import epiandroid.eu.epitech.epiandroid.fragment.TrombiFragment;
import epiandroid.eu.epitech.epiandroid.model.InfoModel;
import epiandroid.eu.epitech.epiandroid.preference.UserPreferenceHelper;
import epiandroid.eu.epitech.epiandroid.utils.Utils;

public class HomeActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView listDrawer;
    private ImageView mPictureView;
    private TextView mLogin, mMail;
    private BaseAdapter navigationDrawerAdapter;
    private List<View> mNavigationArray = new ArrayList<>();
    private View currentSectionSelected = null;
    private int textSectionColor = 0;
    private int tabIndex = 0;
    private InfoModel mInfoModel = null;
    private Fragment currentFragment = null;
    private Bundle basicInfos = new Bundle();
    private String mToken;
    private static AsyncHttpClient mClient = new AsyncHttpClient();
    private String userLogin, userMail;

    public static final String lang = "fra";
    public static final String DATA_PATH = Environment
            .getExternalStorageDirectory().toString() + "/SimpleAndroidOCR/";

    private GsonResponseHandler<InfoModel> mInfoItemGsonResponseHandler = new GsonResponseHandler<InfoModel>(InfoModel.class) {
        @Override
        public void onSuccess(InfoModel infoItem) {
            mInfoModel = infoItem;
            setUpInfoNav(infoItem);
        }

        @Override
        public void onFailure(Throwable throwable, JSONObject errorResponse) {

        }
    };

    private void setUpInfoNav(InfoModel infoItem) {
        mToken = EpitechService.getToken();
        final RequestParams requestParams = new RequestParams();
        Log.d("TOKEN HOME ::", mToken);
        requestParams.add("token", mToken);
        mClient.get("http://epitech-api.herokuapp.com/" + "infos", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject info = response.getJSONObject("infos");
                    userLogin = info.getString("login");
                    userMail = info.getString("internal_email");
                    Log.d("LOGIN HOME ::", userLogin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String urlProfilPicture = "https://cdn.local.epitech.eu/userprofil/profilview/" + userLogin + ".png";
                mLogin.setText(userLogin);
                mMail.setText(userMail);
                Picasso.with(HomeActivity.this)
                        .load(urlProfilPicture)
                        .transform(new CircleTransform())
                        .error(R.drawable.person_image_empty)
                        .into(mPictureView);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("\n======*******========\n", "HomeActivity => onCreate()\n");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        setTessData();

        initView(savedInstanceState);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        initDrawer();
    }

    private void setTessData() {
        String[] paths = new String[] { DATA_PATH, DATA_PATH + "tessdata/" };

        for (String path : paths) {
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Utils.makeText(this, "ERROR: Creation of directory " + path + " on sdcard failed");
                    return;
                }
            }

        }

        // lang.traineddata file with the app (in assets folder)
        // You can get them at:
        // http://code.google.com/p/tesseract-ocr/downloads/list
        if (!(new File(DATA_PATH + "tessdata/" + lang + ".traineddata")).exists()) {
            try {

                AssetManager assetManager = getAssets();
                InputStream in = assetManager.open("tessdata/" + lang + ".traineddata");
                //GZIPInputStream gin = new GZIPInputStream(in);
                OutputStream out = new FileOutputStream(DATA_PATH
                        + "tessdata/" + lang + ".traineddata");

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                //while ((lenf = gin.read(buff)) > 0) {
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                //gin.close();
                out.close();

            } catch (IOException e) {
                Utils.makeText(this, "Was unable to copy " + lang + " traineddata " + e.toString());
            }
        }
    }

    public View builSectionView(int iconRes, int stringRes) {
        View view = LayoutInflater.from(this).inflate(R.layout.navdrawer_section, listDrawer, false);
        ImageView image = (ImageView) view.findViewById(R.id.icon_section);
        TextView text = (TextView) view.findViewById(R.id.text_section);
        image.setImageResource(iconRes);
        text.setText(stringRes);

        return view;
    }

    private void initView(Bundle saveInstanceState) {
        listDrawer = (ListView) findViewById(R.id.navdrawer_listview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        /* set the list for navdrawer */
        mNavigationArray.add(builSectionView(R.drawable.home, R.string.dashboard));
        mNavigationArray.add(builSectionView(R.drawable.marks, R.string.marks));
        mNavigationArray.add(builSectionView(R.drawable.calendar, R.string.calendar));
        mNavigationArray.add(builSectionView(R.drawable.modules, R.string.navdrawer_modules));
//        mNavigationArray.add(builSectionView(R.drawable.trombi, R.string.navdrawer_trombi));
        mNavigationArray.add(builSectionView(R.drawable.settings, R.string.navdrawer_settings));

        navigationDrawerAdapter = new EpiAndroidNavigationAdapter(mNavigationArray);
        listDrawer.setAdapter(navigationDrawerAdapter);

        /* get ui element */
        mPictureView = (ImageView) findViewById(R.id.profile_image);
        mLogin = (TextView) findViewById(R.id.login_textview);
        mMail = (TextView) findViewById(R.id.mail_textview);

        listDrawer.setOnItemClickListener(this);

        EpitechService.postRequest("infos", null, mInfoItemGsonResponseHandler);
        changeSelection(mNavigationArray.get(tabIndex), tabIndex, true);
    }

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.setStatusBarBackgroundColor(
                getResources().getColor(R.color.primaryColorDark));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        Log.d("\n======*******========\n", "onPostCreate() func\n");
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d("\n======*******========\n", "onConfigurationChanged\n");
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("\n======*******========\n", "onOptionsItemSelected() func\n");
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("\n======*******========\n", "OnItemClick : Item change\n");
        changeSelection(view, position, true);
        navigationDrawerAdapter.notifyDataSetChanged();
    }

    public void changeSelection(View view, int position, boolean replaceFragment) {
        Log.d("\n======*******========\n", "changeSelection\n");
        ImageView imageView = (ImageView) view.findViewById(R.id.icon_section);
        TextView textView = (TextView) view.findViewById(R.id.text_section);

        tabIndex = position;
        if (view == currentSectionSelected) {
            return;
        }

        if (currentSectionSelected != null) {
            ImageView oldImageView = (ImageView) currentSectionSelected.findViewById(R.id.icon_section);
            TextView oldTextView = (TextView) currentSectionSelected.findViewById(R.id.text_section);
            oldImageView.clearColorFilter();
            oldTextView.setTextColor(textSectionColor);
        } else {
            textSectionColor = textView.getCurrentTextColor();
        }
        imageView.setColorFilter(getResources().getColor(R.color.primaryColor));
        textView.setTextColor(getResources().getColor(R.color.primaryColor));
        currentSectionSelected = view;

        if (replaceFragment == false) {
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = null;

        /* change fragment */
        switch (position) {
            case 0:
                Log.d("\n======*******========\n", "SWITCH : Enter in dashboard frag\n");
                fragment = new DashboardFragment();
                toolbar.setTitle(getResources().getString(R.string.dashboard));
                break;
            case 1:
                Log.d("\n======*******========\n","SWITCH : Enter in marks frag\n");
                fragment = new MarksFragment();
                toolbar.setTitle(getResources().getString(R.string.marks));
                break;
            case 2:
                fragment = new PlanningFragment();
                toolbar.setTitle(getResources().getString(R.string.navdrawer_planning));
                break;
            case 3:
                fragment = new ModulesFragment();
                toolbar.setTitle(getResources().getString(R.string.navdrawer_modules));
                break;
            case 4:
                fragment = new TrombiFragment();
                toolbar.setTitle(getResources().getString(R.string.navdrawer_trombi));
                //Log.d("TITLE ===========> ", basicInfos.getString("TITLE"));
                //Log.d("TOKEN ===========> ", basicInfos.getString("TOKEN"));
                break;
            case 5:
                fragment = new SettingsFragment();
                toolbar.setTitle(getResources().getString(R.string.navdrawer_settings));
                break;
        }
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName()).commit();
        }
        currentFragment = fragment;
        drawerLayout.closeDrawers();
    }

    public void setBundle(Bundle basicInfos)
    {
        this.basicInfos = basicInfos;
    }

    public void logout(View view) {
        Log.d("\n======*******========\n", "logout() FUNC\n");
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(R.string.logout)
                .setMessage(R.string.really_logout)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        UserPreferenceHelper.logout(HomeActivity.this);
                        HomeActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }
}