package epiandroid.eu.epitech.epiandroid.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import epiandroid.eu.epitech.epiandroid.R;

/**
 * Created by Steeven on 25/01/2016.
 */

//TODO: rediriger vers l'activiter login a la deconnexion
//Code couleur des l'icone : #2DA0ED
//http://www.flaticon.com/free-icon/photo-camera_62908 -- credit icones
//a telecharger en 128px

public class TrombiFragment extends android.support.v4.app.Fragment {

	private Activity mActivity;


	// https://cdn.local.epitech.eu/userprofil/trombiview/brunne_s.jpg
	// android:src="@drawable/logout"

	public interface OnDataPass {
		public void onDataPass(String data);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.trombi_list, container, false);
		getImagesList();

		// Inflate the layout for this fragment
		return rootView;
	}

	public void getImagesList()
	{
		//
		List<String> myList = null; //peupler la liste

		fillView(myList);
	}

	public void fillView(List<String> url)
	{

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}
}
