package epiandroid.eu.epitech.epiandroid.model;

import com.google.gson.annotations.SerializedName;

public class UserItem {
    @SerializedName("picture")
    public String picture;

    @SerializedName("title")
    public String title;

    @SerializedName("url")
    public String url;

}
